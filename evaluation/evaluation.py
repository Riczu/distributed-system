"""This contains the evaluation part of the project.
Mean and standard deviation values have calculated from all responses
and data has been plotted to show the behaviour of the data for the report."""

import matplotlib.pyplot as plt
import numpy as np


if __name__ == "__main__":

    with open('registration_delays.txt') as f:
        lines = f.readlines()

    registration_delays = []
    for item in lines:
        item = item.rstrip("\n")
        registration_delays.append(float(item))

    with open('registration_delays_hash.txt') as f:
        lines = f.readlines()

    registration_delays_hash = []
    for item in lines:
        item = item.rstrip("\n")
        registration_delays_hash .append(float(item))

    with open('publish_delays.txt') as f:
        lines = f.readlines()

    publish_delays = []
    for item in lines:
        item = item.rstrip("\n")
        publish_delays.append(float(item))


    with open('publish_delays_hash.txt') as f:
        lines = f.readlines()

    publish_delays_hash = []
    for item in lines:
        item = item.rstrip("\n")
        publish_delays_hash.append(float(item))

    with open('subscribe_delays.txt') as f:
        lines = f.readlines()

    subscribe_delays = []
    for item in lines:
        item = item.rstrip("\n")
        subscribe_delays.append(float(item))

    with open('subscribe_delays_hash.txt') as f:
        lines = f.readlines()

    subscribe_delays_hash = []
    for item in lines:
        item = item.rstrip("\n")
        subscribe_delays_hash.append(float(item))

    print(registration_delays)
    print(registration_delays_hash)
    print(publish_delays)
    print(publish_delays_hash)
    print(subscribe_delays)
    print(subscribe_delays_hash)

    registration_delays = np.array(registration_delays)
    registration_delays_hash = np.array(registration_delays_hash)

    publish_delays = np.array(publish_delays)
    publish_delays_hash = np.array(publish_delays_hash)

    subscribe_delays = np.array(subscribe_delays)
    subscribe_delays_hash = np.array(subscribe_delays_hash)

    print("")
    print("registration_mean: " + str(np.mean(registration_delays)))
    print("registration_hash_mean: " + str(np.mean(registration_delays_hash)))
    print("")
    print("publish_move_mean: " + str(np.mean(publish_delays)))
    print("publish_move_hash_mean: " + str(np.mean(publish_delays_hash)))
    print("")
    print("subscribe_move_mean: " + str(np.mean(subscribe_delays)))
    print("subscribe_hash_mean: " + str(np.mean(subscribe_delays_hash)))

    print("")
    print("registration_std: " + str(np.std(registration_delays)))
    print("registration_hash_std: " + str(np.std(registration_delays_hash)))
    print("")
    print("publish_move_std: " + str(np.std(publish_delays)))
    print("publish_move_hash_std: " + str(np.std(publish_delays_hash)))
    print("")
    print("subscribe_move_std: " + str(np.std(subscribe_delays)))
    print("subscribe_move_hash_std: " + str(np.std(subscribe_delays_hash)))


    x = range(1, len(registration_delays) + 1)
    plt.title("Registration Delays")
    plt.xlabel("Sample")
    plt.ylabel("Time (s)")

    plt.plot(x, registration_delays, "coral", label="registration")
    plt.plot(x, registration_delays_hash, "cornflowerblue", label="registration_hash")
    plt.legend(loc="best")
    plt.xticks(range(1, len(registration_delays) + 1))

    plt.show()


    x = range(1, len(publish_delays) + 1)
    plt.title("Publish Delays")
    plt.xlabel("Sample")
    plt.ylabel("Time (s)")

    plt.plot(x, publish_delays, "coral", label="publish_move")
    plt.plot(x, publish_delays_hash, "cornflowerblue", label="publish_move_hash")
    plt.legend(loc="best")
    plt.xticks(range(1, len(publish_delays) + 1))

    plt.show()


    x = range(1, len(subscribe_delays) + 1)
    plt.title("Subscribe Delays")
    plt.xlabel("Sample")
    plt.ylabel("Time (s)")

    plt.plot(x, subscribe_delays, "coral", label="subscribe_move")
    plt.plot(x, subscribe_delays_hash, "cornflowerblue", label="subscribe_move_hash")
    plt.legend(loc="best")
    plt.xticks(range(1, len(subscribe_delays) + 1))

    plt.show()
