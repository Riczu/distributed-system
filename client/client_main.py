"""Module for client implementation."""
import copy
import hashlib
import random
import sys
import time
import uuid
import requests


class Client:
    """Class for Player (client)."""
    def __init__(self):
        #self.player_id = str(uuid.uuid4())[:6]
        self.player_id = hashlib.sha256(str(uuid.uuid4())[:6].encode()).hexdigest()
        self.game_id = None
        self.register_address = "http://server/register"
        self.pub_sub_address = "http://server/game/"
        self.player_cards = []
        self.player_status = None
        self.registration_delays = []
        self.publish_delays = []
        self.subscribe_delays = []

    def in_fun(self):
        """Subscribe to tuple space (perform in)."""
        response = requests.get(self.pub_sub_address + self.game_id + "?player_id=" + self.player_id)
        self.subscribe_delays.append(response.elapsed.total_seconds())
        # print(self.player_id + " " + str(response.elapsed.total_seconds()))
        return response

    def out_fun(self, move):
        """Publish to tuple space (perform out)."""
        data = {"move": move}
        response = requests.post(self.pub_sub_address + self.game_id + "?player_id=" + self.player_id, json=data)
        self.publish_delays.append(response.elapsed.total_seconds())
        # print(self.player_id + " " + str(response.elapsed.total_seconds()))
        return response

    def choose_move(self):
        """Choose the next move."""
        # moves = ["HIT", "STAND"]
        # next_move = random.choice(moves)
        modifiable_cards = copy.deepcopy(self.player_cards)
        next_move = "HIT"
        if sum(self.player_cards) > 17:
            for card in modifiable_cards:
                if card == 11:
                    if self.check_over_21(modifiable_cards):
                        modifiable_cards[modifiable_cards.index(card)] = 1
                if sum(modifiable_cards) >= 17:
                    next_move = "STAND"
        return next_move

    @staticmethod
    def check_over_21(cards):
        """Check if the sum of cards exceeds 21."""
        if sum(cards) > 21:
            return True
        return False

    def _game_logic(self):
        """Perform actual logic."""
        if self.player_status == "WIN":
            print("Yeah!!!")
        else:
            while True:
                response = None
                time.sleep(2)
                move = self.choose_move()
                print("Next move: {} ...".format(move))
                status_code = 400
                while status_code != 200:
                    response = self.out_fun(move)
                    status_code = response.status_code
                status_code = 400
                while status_code != 200:
                    response = self.in_fun()
                    status_code = response.status_code
                data = response.json()
                self.player_status = data["message"]["status"]
                if data["message"]["card"] is not None:
                    self.player_cards.append(data["message"]["card"])
                print("Current cards: {}, status: {} ".format(self.player_cards, self.player_status))
                if data["message"]["status"] == "WIN":
                    print("Yeah!!!")
                    self.save_response_delays()
                    sys.exit()
                if data["message"]["status"] == "LOSE":
                    print("Ohh crap!!!")
                    self.save_response_delays()
                    sys.exit()

    def register_to_game(self):
        """Register to the game by sending the player id to the server."""
        print("Registering for a game...")
        response = requests.post(self.register_address + "?player_id=" + self.player_id)
        self.registration_delays.append(response.elapsed.total_seconds())
        # print(self.player_id + " " + str(response.elapsed.total_seconds()))
        status = False
        if response.status_code == 200:
            status = True
        return status, response

    def parse_game_setup(self, response):
        """Parse the game setup from the registration response."""
        data = response.json()
        self.game_id = data["game_id"]
        self.player_cards.append(data["player_card_1"])
        self.player_cards.append(data["player_card_2"])
        self.player_status = data["status"]
        print("Game setup: Player cards: {} Game status: {}".format(self.player_cards, self.player_status))

    def save_response_delays(self):
        """Save all response delays after the game end"""
        with open("client/registration_delays_hash.txt", 'a') as f:
            for item in self.registration_delays:
                f.write("{}\n".format(item))

        with open("client/publish_delays_hash.txt", 'a') as f:
            for item in self.publish_delays:
                f.write("{}\n".format(item))

        with open("client/subscribe_delays_hash.txt", 'a') as f:
            for item in self.subscribe_delays:
                f.write("{}\n".format(item))

    def main(self):
        """Main logic."""
        response = None
        registered = False
        print("Player ID:", self.player_id)
        while not registered:
            time.sleep(3)
            registered, response = self.register_to_game()
        self.parse_game_setup(response)
        self._game_logic()


if __name__ == "__main__":
    client = Client()
    time.sleep(2)
    client.main()
