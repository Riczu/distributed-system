"""Module for blackjack game server API."""
import json
from flask import Flask, request

from server import Server

app = Flask(__name__)
server = Server()


@app.route("/register", methods=['POST'])
def register_player():
    """Register player to the game"""
    try:
        player_id = str(request.args["player_id"])
    except KeyError:
        return "Missing query parameter: player_id", 400
    game_data = server.add_player(player_id)
    return json.dumps(game_data), 200


@app.route("/game/<game_id>", methods=['GET'])
def subscribe_move(game_id):
    """Player subscribes to the game id, return the game status."""
    try:
        player_id = str(request.args["player_id"])
    except KeyError:
        return "Missing query parameter: player_id", 400

    if game_id != server.game_id:  # not in server.get_game_ids():
        return "The game not exist", 404

    if not server.check_player_id(player_id):
        return "Invalid player ID.", 403

    move_tuple = server.get_move(game_id, player_id)
    return json.dumps(move_tuple), 200


@app.route("/game/<game_id>", methods=['POST'])
def publish_move(game_id):
    """Player publishes the move to the tuple space."""
    try:
        player_id = str(request.args["player_id"])
        data = request.get_json()
    except KeyError:
        return "Missing query parameter: player_id", 400

    except ValueError:
        return "Invalid body type", 400

    if game_id != server.game_id:  # not in server.get_game_ids():
        return "The game not exist", 404

    if not server.check_player_id(player_id):
        return "Invalid player ID.", 403

    try:
        move = data["move"]
        if move not in ("HIT", "STAND"):
            return "Invalid move", 400
    except KeyError:
        return "Move is missing", 400

    move_tuple = (player_id, game_id, move)
    server.add_move(move_tuple, player_id)

    return "Move received", 200


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
