"""Module for tuple space implementation.

The tuple space is implemented as a list of tuples.
Tuple space (Linda) includes three operations:
- read (read a tuple from tuple space without removing the tuple)
- in (read a tuple from tuple space and remove the tuple)
- out (add a tuple to the tuple space)
"""


class TupleSpace:
    """Class for TupleSpace."""
    def __init__(self):
        self._tuple_space = []

    @property
    def tuple_space(self):
        """Getter for tuple_space."""
        return self._tuple_space

    def in_fun(self, topic):
        """Function for tuple space in-functionality.
        Read tuple from the tuple space and remove the tuple.
        """
        for tpl in self._tuple_space:
            if tpl[1] == topic:
                idx = self._tuple_space.index(tpl)
                return self._tuple_space.pop(idx)
        return None

    def rd_fun(self, topic):
        """Function for tuple space read-functionality.
        Read from the tuple space without removing the tuple.
        """
        for tpl in self._tuple_space:
            if tpl[1] == topic:
                return tpl
        return None

    def out_fun(self, tpl):
        """Function for tuple space read-functionality.
        Add a tuple to the tuple space.
        """
        self._tuple_space.append(tpl)
