"""File contains Server class and functionalities be as a dealer in game"""
import copy
import hashlib
import uuid

from deck import Deck
from tuple_space import TupleSpace


class Server:
    """Server class for the game"""
    def __init__(self):
        #self.game_id = str(uuid.uuid4())[:6]
        self.game_id = hashlib.sha256(str(uuid.uuid4())[:6].encode()).hexdigest()
        self.all_players = []
        self.active_players = []
        self.deck = Deck()
        self.dealer_cards = [self.deck.draw_card(), self.deck.draw_card()]
        self.tuple_space = TupleSpace()
        self.player_cards = []
        self.player_turn = None

    def check_game_end(self, player_id):
        """Check whether game continues, or the player loses or wins."""
        end = "ONGOING"
        cards = []
        for player in self.player_cards:
            if player['player_id'] == player_id:
                cards = player['cards']

        if self.check_greater_than_21(cards):
            for card in cards:
                if card == 11:
                    idx = cards.index(card)
                    cards[idx] = 1
                    if not self.check_greater_than_21(cards):
                        break

        if sum(cards) == 21:
            print("Sum of player cards equals to 21.")
            self.active_players.pop(self.active_players.index(player_id))
            return "WIN"
        if self.check_greater_than_21(cards):
            print("Sum of player cards exceeds 21.")
            self.active_players.pop(self.active_players.index(player_id))
            return "LOSE"
        return end

    @staticmethod
    def check_greater_than_21(cards):
        """Check whether the sum of cards is greater than 21."""
        return sum(cards) > 21

    def check_player_cards_greater_than_dealer_cards(self, player_id):
        """Check whether the sum of player cards is greater than the sum of dealer cards."""
        self.active_players.pop(self.active_players.index(player_id))
        cards = []
        for player in self.player_cards:
            if player['player_id'] == player_id:
                cards = player['cards']
        sum_dealer = sum(self.dealer_cards)
        if sum_dealer < sum(cards):
            print("Game ends: sum of player cards greater than the sum of dealer cards.")
            return "WIN"
        print("Game ends: sum of player cards smaller than the sum of dealer cards.")
        return "LOSE"

    def add_player(self, player_id):
        """Register player to the game.
        Create the setup for the player.
        """
        self.all_players.append(player_id)
        self.active_players.append(player_id)
        card_1 = self.deck.draw_card()
        card_2 = self.deck.draw_card()
        player_cards_obj = {
            "player_id": player_id,
            "cards": [card_1, card_2]
        }
        self.player_cards.append(player_cards_obj)
        end = self.check_game_end(player_id)
        cards = {
            "game_id": self.game_id,
            "player_card_1": card_1,
            "player_card_2": card_2,
            "dealer_cards": [self.dealer_cards[0]],
            "status": end
        }
        if len(self.all_players) == 1:
            print("Adding first player: {}".format(player_id), flush=True)
            if end != "WIN":
                self.player_turn = player_id
            print("Player: {} started with status: {}, current turn: {}".format(player_id, end, self.player_turn), flush=True)
        else:
            print("Adding second player: {}".format(player_id), flush=True)
            if len(self.active_players) == 1:
                print("Only one active player: {}".format(self.active_players[0]))
                if self.active_players[0] == player_id:
                    print("Giving turn to the second player: {}".format(self.active_players))
                    self.player_turn = player_id
            print("Player: {} started with status: {}, current turn: {}".format(player_id, end, self.player_turn), flush=True)
        print("Current active players: {}".format(self.active_players))
        return cards

    def check_player_id(self, player_id):
        """Check whether it's the player's turn."""
        if player_id in self.all_players:
            if self.player_turn == player_id:
                return True
        return False

    def get_move(self, game_id, player_id):
        """Get a move from the tuple space.
        Update active players based on the conditions.
        """
        print("Returning game status from the tuple space...")
        players = copy.deepcopy(self.all_players)
        if len(self.active_players) == 1:
            if player_id not in self.active_players:
                players.pop(players.index(player_id))
                self.player_turn = players[0]
        if len(self.active_players) > 1:
            players.pop(players.index(player_id))
            self.player_turn = players[0]
        tpl = self.tuple_space.in_fun(game_id)
        tpl_dict = {
            "sender": tpl[0],
            "game_id": tpl[1],
            "message": tpl[2]
        }
        return tpl_dict

    def add_move(self, move_tuple, player_id):
        """Add a move to the tuple space.
        Check game conditions.
        """
        print("Adding the move from player (ID: {}) to the tuple space...".format(player_id))
        self.tuple_space.out_fun(move_tuple)
        data = self.tuple_space.in_fun(self.game_id)
        move = data[2]
        server_tuple = None
        print("Checking player (ID: {}) game status...".format(player_id))
        if move == 'STAND':
            end = self.check_player_cards_greater_than_dealer_cards(player_id)
            message = {"status": end, "card": None}
            server_tuple = ("Server", self.game_id, message)
        if move == 'HIT':
            card = self.deck.draw_card()
            for player in self.player_cards:
                if player['player_id'] == player_id:
                    player['cards'].append(card)
            end = self.check_game_end(player_id)
            if end in ("WIN", "LOSE"):
                message = {"status": end, "card": card}
                server_tuple = ("Server", self.game_id, message)
            else:
                message = {"status": "ONGOING", "card": card}
                server_tuple = ("Server", self.game_id, message)

        self.tuple_space.out_fun(server_tuple)
