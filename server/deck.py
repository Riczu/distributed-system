"""Module for a card deck implementation."""
import random


class Deck:
    """Class for a card deck model."""
    def __init__(self):
        self.deck = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11]*4
        self.shuffle()

    def shuffle(self):
        """Shuffle the cards of the deck."""
        random.shuffle(self.deck)

    def draw_card(self):
        """Draw a card from the deck."""
        return self.deck.pop()
